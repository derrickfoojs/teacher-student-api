var db = require('../dbconnection'); //reference of dbconnection.js
var query = "";

var Teacher = {

    getStudentsByTeacher: function (teacher, callback) {
        if (Array.isArray(teacher)) {
            query = db.query("select s.student AS students from Student s, Teacher t where s.teacher_id=t.id AND t.teacher in (?) AND status='R' GROUP BY s.student HAVING COUNT(1) > 1", [teacher], callback);
        } else {
            query = db.query("select s.student AS students from Student s, Teacher t where s.teacher_id=t.id AND t.teacher in (?) AND status='R'", [teacher], callback);
        }
        return query;
    },
    addStudentTeacher: function (Teacher, callback) {
        if (Array.isArray(Teacher.students) && Teacher.students.length) {
            for (var i = 0; i < Teacher.students.length; i++) {
                query = db.query("INSERT INTO Student(teacher_id, student, status) values ((select distinct id from Teacher where teacher=?),?,'R')", [Teacher.teacher, Teacher.students[i]]);
            }
        } else {
            query = db.query("INSERT INTO Student(teacher_id, student, status) values ((select distinct id from Teacher where teacher=?),?,'R')", [Teacher.teacher, Teacher.students]);
        }
        callback(null, query);
        return query;
    },
    updateStudents: function (Teacher, callback) {
        console.log(Teacher);
        if (Array.isArray(Teacher.students) && Teacher.students.length) {
            for (var i = 0; i < Teacher.students.length; i++) {
                query = db.query("update Student set status='S' where student=?", [Teacher.students[i]]);
            }
        } else {
            query = db.query("update Student set status='S' where student=?", [Teacher.students]);
        }
        callback(null, query);
        return query;
    },
    retrieveStudentsForNotifications: function (Teacher, callback) {
            query = db.query("SELECT s.student as recipients from Student s, Teacher t where t.id=s.teacher_id AND t.teacher=? AND s.status!='S'", [Teacher.teacher], callback);
        return query;
    }

};
module.exports = Teacher;
