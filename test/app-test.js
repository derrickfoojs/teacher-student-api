var expect = require('chai').expect;
var Teacher = require('../models/teacher');
var chai = require('chai');
var chaiHttp = require('chai-http')

chai.should();
chai.use(chaiHttp);

var host = "http://localhost:3000"
var teacherName = "teachertest@example.com";
var teacherNameArray = [ "teacherken@example.com", "teacherjoe@example.com" ];
var registerRequestBody = {
    "teacher": "teachertestregister@example.com",
    "students": ["example1@gmail.com", "example2@gmail.com"]
};

var suspendRequestBody = {
    "students": ["example1@gmail.com", "example2@gmail.com"]
};

var notificationRequestBody = {
    "teacher":  "teachertestregister@example.com",
    "notification": "Hello students! @studentagnes@example.com @studentmiche@example.com"
};

describe('First test', () => {
    it('should assert true to be true', function () {
        expect(true).to.be.true;
    });
});

describe('Checking students under teachertest', () => {

    it('should return students that belong to teachertest', (done) => {
        Teacher.getStudentsByTeacher(teacherName, function (err, rows) {
            expect(rows[0].students).to.equal("teachertest@gmail.com");
            expect(rows[1].students).to.equal("teachertest2@gmail.com");
            expect(rows[2].students).to.equal("student_only_under_teacher_test@gmail.com");
            done();
        });
    });

    it('should return common students that belong to teacherken and teacherjoe', (done) => {
        Teacher.getStudentsByTeacher(teacherNameArray, function (err, rows) {
            expect(rows[0].students).to.equal("commonstudent1@example.com");
            expect(rows[1].students).to.equal("commonstudent2@example.com");
            done();
        });
    });
});

describe('Registering students under teachertestregister', () => {
    it('should register students to teachertestregister', (done) => {
        chai.request(host)
            .post('/api/register')
            .set('content-type', 'application/json')
            .send(registerRequestBody)
            .end((error, res) => {
                if (error) {
                    done(error);
                } else {
                    res.should.have.status(204);
                    done();
                }
        });
    });
});

describe('Suspending students under teachertest', () => {
    it('should register students to teacher test', (done) => {
        chai.request(host)
            .post('/api/suspend')
            .set('content-type', 'application/json')
            .send(suspendRequestBody)
            .end((error, res) => {
                if (error) {
                    done(error);
                } else {
                    res.should.have.status(204);
                    done();
                }
        });
    });
});

describe('Retrieiving notifications for students under teacher register test', () => {
    it('should register students to teacher test', (done) => {
        chai.request(host)
            .post('/api/retrievefornotifications')
            .set('content-type', 'application/json')
            .send(notificationRequestBody)
            .end((error, res) => {
                if (error) {
                    done(error);
                } else {
                    res.should.have.status(200);
                    res.body.should.have.property('recipients');
                    expect(res.body.recipients.concat()).to.contain('studentagnes@example.com');
                    expect(res.body.recipients.concat()).to.contain('studentmiche@example.com');
                    done();
                }
        });
    });
});
