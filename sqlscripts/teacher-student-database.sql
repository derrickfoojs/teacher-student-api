create database if not exists teacher_student;

use teacher_student;

CREATE TABLE IF NOT EXISTS Student
(
  id         int auto_increment
    primary key,
  teacher_id int          not null,
  student    varchar(255) not null,
  status     varchar(10)  not null
);

CREATE TABLE IF NOT EXISTS  Teacher
(
  id      int auto_increment
    primary key,
  teacher varchar(255) not null
);


INSERT INTO teacher_student.Teacher (id, teacher) VALUES (1, 'teacherken@example.com');
INSERT INTO teacher_student.Teacher (id, teacher) VALUES (2, 'teacherjoe@example.com');
INSERT INTO teacher_student.Teacher (id, teacher) VALUES (3, 'teachertest@example.com');
INSERT INTO teacher_student.Teacher (id, teacher) VALUES (4, 'teachertestregister@example.com');

INSERT INTO teacher_student.Student (id, teacher_id, student, status) VALUES (1, 1, 'studentjon@example.com', 'R');
INSERT INTO teacher_student.Student (id, teacher_id, student, status) VALUES (2, 1, 'studenthon@example.com', 'R');
INSERT INTO teacher_student.Student (id, teacher_id, student, status) VALUES (3, 1, 'student_only_under_teacher_ken@example.com', 'R');
INSERT INTO teacher_student.Student (id, teacher_id, student, status) VALUES (4, 1, 'commonstudent1@example.com', 'R');
INSERT INTO teacher_student.Student (id, teacher_id, student, status) VALUES (5, 1, 'commonstudent2@example.com', 'R');
INSERT INTO teacher_student.Student (id, teacher_id, student, status) VALUES (6, 2, 'commonstudent1@example.com', 'R');
INSERT INTO teacher_student.Student (id, teacher_id, student, status) VALUES (7, 2, 'commonstudent2@example.com', 'R');
INSERT INTO teacher_student.Student (id, teacher_id, student, status) VALUES (8, 3, 'teachertest@gmail.com', 'R');
INSERT INTO teacher_student.Student (id, teacher_id, student, status) VALUES (9, 3, 'teachertest2@gmail.com', 'R');
INSERT INTO teacher_student.Student (id, teacher_id, student, status) VALUES (10, 3, 'student_only_under_teacher_test@gmail.com', 'R');

commit;