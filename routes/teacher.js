var express = require('express');
var router = express.Router();
var Teacher = require('../models/Teacher');

router.get('/commonstudents', function (req, res) {
    if (req.query) {
        if (req.query.teacher == null || req.query.teacher == '') {
            res.status(404).json("{ \"message\": \"Students is empty\" }");
        } else {
            Teacher.getStudentsByTeacher(req.query.teacher, function (err, rows) {
                if (err) {
                    res.status(404).json(err);
                } else {
                    var results = {students: []};
                    for (var i = 0; i < rows.length; i++) {
                        results.students = results.students.concat(rows[i].students);
                    }
                    res.status(200).json(results);
                }
            });
        }
    } else {
        res.status(404).send("Not found");
    }
});
router.post('/register', function (req, res) {
    Teacher.addStudentTeacher(req.body, function (err) {
                if (err) {
                    res.status(404).json(err);
        } else {
            if (req.body.students.length > 1) {
                res.status(204).json(req.body);
            } else {
                res.status(404).json("{ \"message\": \"Students is empty\" }");
            }
        }
    });
});
router.post('/suspend', function (req, res) {

    Teacher.updateStudents(req.body, function (err, rows) {
        if (err) {
            res.status(404).json(err);
        } else if (Object.keys(req.body).length > 1) {
            res.status(404).json("{ \"message\": \"Wrong request received\" }")
        } else {
            if (req.body.students.length > 1) {
                res.status(204).json(req.body);
            } else {
                res.status(404).json("{ \"message\": \"Students is empty\" }");
            }
        }
    });
});

router.post('/retrievefornotifications', function (req, res) {
    Teacher.retrieveStudentsForNotifications(req.body, function (err, rows) {
        if (err) {
            res.status(404).json(err);
        } else if (Object.keys(req.body).length > 2) {
            res.status(404).json("{ \"message\": \"Wrong request received\" }")
        } else {
            if (req.body.notification.length > 1) {
                var email = req.body.notification.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi);
                var results = {recipients: []};
                for (var i = 0; i < rows.length; i++) {
                    results.recipients = results.recipients.concat(rows[i].recipients);
                }
                if (email != null && email.length > 0) {
                    results.recipients = results.recipients.concat(email)
                }
                results.recipients = results.recipients.filter((v,i) => results.recipients.indexOf(v) === i);
                if(results.recipients.length>1) {
                    res.status(200).json(results);
                } else {
                    res.status(404).json("{ \"message\": \"Recipients is empty\" }");
                }
            } else {
                res.status(404).json("{ \"message\": \"Recipients is empty\" }");
            }
        }
    });
});

module.exports = router;
