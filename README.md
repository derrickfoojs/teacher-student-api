# Teacher Student API

### Installation
**MySql:** 

Connect to DB with `mysql -uroot`

Give access to root for privileges

`GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost WITH GRANT OPTION;`

Create DB

`CREATE DATABASE teacher_student;`

Execute the file `./sqlscripts/teacher-student-database.sql`

**NPM:**
Run npm install

Run npm start

Ensure that database connection is setup and localhost is port 3000

Run npm test on another terminal
